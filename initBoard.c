#include <stdio.h>

#include "BackGammon.h"

void initBoard(board *brdp) {
    int i, point = 0;
    int index1 = 0;
    int index2 = 15;

    // Piecesの初期設定
    while (1) {
        switch (point) {
            case 1:
                for (i = 0; i < 2; i++) {
                    brdp->pieces[index2].x = point;
                    brdp->pieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 6:
                for (i = 0; i < 5; i++) {
                    brdp->pieces[index1].x = point;
                    brdp->pieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 8:
                for (i = 0; i < 3; i++) {
                    brdp->pieces[index1].x = point;
                    brdp->pieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 12:
                for (i = 0; i < 5; i++) {
                    brdp->pieces[index2].x = point;
                    brdp->pieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 13:
                for (i = 0; i < 5; i++) {
                    brdp->pieces[index1].x = point;
                    brdp->pieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 17:
                for (i = 0; i < 3; i++) {
                    brdp->pieces[index2].x = point;
                    brdp->pieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 19:
                for (i = 0; i < 5; i++) {
                    brdp->pieces[index2].x = point;
                    brdp->pieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 24:
                for (i = 0; i < 2; i++) {
                    brdp->pieces[index1].x = point;
                    brdp->pieces[index1].player = SENTE;
                    index1++;
                }
                break;
        }

        if (point == 24) {
            break;
        }
        point++;
    }
    // Playersの初期設定
    brdp->Players[1].direction = GOTE;
    brdp->Players[0].direction = SENTE;
    brdp->Players[1].score = 0;
    brdp->Players[0].score = 0;
    brdp->Players[1].ownedPieceFlag = 0;
    brdp->Players[0].ownedPieceFlag = 0;

    // blockFlagの初期設定
    blockPoint(brdp);


    // userInputの初期設定
    brdp->input.uinMoveNum = 0;
    brdp->input.uinPoint = 0;
    // turnの初期設定
    brdp->turn = SENTE;

    // diceの初期設定
    brdp->dices[0].diceNum = 0;
    brdp->dices[0].diceflag = 0;
    brdp->dices[0].doubletflag = 0;
    brdp->dices[1].diceNum = 0;
    brdp->dices[1].diceflag = 0;
    brdp->dices[1].doubletflag = 0;
    
    //board構造体の中身を変更
    brdp->winner = 0;
    brdp->diceIndex = 0;
}
