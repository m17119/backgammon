#include <stdio.h>

#include "BackGammon.h"
void checkScore(board *brdp){

	int i;
    int scount = 0;
    int gcount = 0;

    for (i = 0; i < MAX_PIECES; i++) {
    	
        if (i < 15) {
            if (brdp->pieces[i].x == 0) {
               scount++;
            }
        } else {
            if (brdp->pieces[i].x == 25) {
               gcount++; 
            }
        }
       
    }
    
    brdp->Players[0].score=scount;
    brdp->Players[1].score=gcount;

    	if(scount == 15){
            brdp->winner=1;
        }else if(gcount == 15){
            brdp->winner=-1;
        }else{
        	brdp->winner=0;
        }

        
            
}