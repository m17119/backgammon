#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testDisplayBoard() {
    testStart("displayBoard");
    board brd, abrd;

    initscr();

    //初期配置
    initBoard(&brd);
    initBoard(&abrd);
    rollDice(brd.dices);
    abrd.dices[0] = brd.dices[0];
    abrd.dices[1] = brd.dices[1];
    displayBoard(&brd);
    assertEqualsInt(isEquals(&brd, &abrd), 1);

    //持ち駒があるとき
    setBoardByMaster(&brd, SENTE, "......5.3....5..........11",
                     "11..........5....3.5......");
    setBoardByMaster(&abrd, SENTE, "......5.3....5..........11",
                     "11..........5....3.5......");
    rollDice(brd.dices);
    abrd.dices[0] = brd.dices[0];
    abrd.dices[1] = brd.dices[1];
    displayBoard(&brd);
    assertEqualsInt(isEquals(&brd, &abrd), 1);

    //ゴールに駒がある時
    setBoardByMaster(&brd, SENTE, "F.........................",
                     ".........................F");
    setBoardByMaster(&abrd, SENTE, "F.........................",
                     ".........................F");
    rollDice(brd.dices);
    abrd.dices[0] = brd.dices[0];
    abrd.dices[1] = brd.dices[1];
    displayBoard(&brd);
    assertEqualsInt(isEquals(&brd, &abrd), 1);

    // cursesの終了.
    endwin();
}