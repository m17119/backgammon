#ifndef __BackGammon_h
#define __BackGammon_h
#define MAX_PIECES 30
#define MAX_PLAYERS 2
#define MAX_POINT 26
#define MAX_DICE 2

#include <stdbool.h>

#ifdef __CYGWIN__
#include <ncurses.h>
#elif _WIN64
#include "PDCurses-3.9/curses.h"
#elif _WIN32
#include "PDCurses-3.9/curses.h"
#else
#include <ncurses.h>
#endif

typedef enum { SENTE = 1, GOTE = -1 } playerType;  // directionと同じ値に定義.
// typedef enum { BLACK, WHITE } pieceTypeE; 必要なし！

typedef struct {
    int uinMoveNum;
    int uinPoint;
} userinput;

typedef struct {
    int direction;
    char name[255];
    int score;
    int ownedPieceFlag;
} player;

typedef struct {
    int x;
    // pieceTypeE type; 必要なし！
    playerType player;
} piece;

typedef struct {
    int diceNum;
    int diceflag;
    int doubletflag;
} dice;

typedef struct {
    piece pieces[MAX_PIECES];
    player Players[MAX_PLAYERS];
    int blockFlag[MAX_POINT];
    userinput input;
    playerType turn;
    dice dices[MAX_DICE];
    int winner;
    int diceIndex;
    int bdice[2];
} board;

// **** プロトタイプ宣言集

int setPiece(piece *piep, int x, char num, int startNum);
int canMove(board *brdp);
void resetPiece(board *brdp);
void checkScore(board *brdp);
void setTurn(board *brdp, char *nameA, char *nameB);
int isEquals(board *brdp, board *abrdp);
int canGoal(piece *piep, int turn);
void rollDice(dice *dicp);
void setBoardByMaster(board *brdp, playerType turn, char *skp, char *gkp);
void blockPoint(board *brdp);
void checkDice(dice *dicp, userinput uin, int *index);
void checkOwnPiece(piece *piep, player *plap);
void useDice(dice *dicp, int index);
void initBoard(board *brdp);
int axisMove(board *brdp);
void goalMove(board *brdp);
void displayBoard(board *brdp);

#endif
