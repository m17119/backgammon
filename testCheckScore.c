#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testCheckScore() {
    testStart("checkScore");
    board brd;

    //未勝利の時(ゴールになにも入っていない)
    initBoard(&brd);
    checkScore(&brd);
    assertEqualsInt(0, brd.winner);
    assertEqualsInt(0, brd.Players[0].score);
    assertEqualsInt(0, brd.Players[1].score);
    
    //未勝利の時(ゴールに少し入っている)
    setBoardByMaster(&brd, SENTE, "2..5...3...5..............",
    "............5....2.5.....3");
    checkScore(&brd);
    assertEqualsInt(0, brd.winner);
    assertEqualsInt(2, brd.Players[0].score);
    assertEqualsInt(3, brd.Players[1].score);

    //SENTEが勝利の時
    setBoardByMaster(&brd, SENTE, "F.......................",
    ".2..........5....3.5......");
    checkScore(&brd);
    assertEqualsInt(1, brd.winner);
    assertEqualsInt(15, brd.Players[0].score);
    assertEqualsInt(0, brd.Players[1].score);
    
    //GOTEが勝利の時
    setBoardByMaster(&brd, GOTE, ".2..........5....3.5......",
    ".........................F");
    checkScore(&brd);
    assertEqualsInt(-1, brd.winner);
    assertEqualsInt(0, brd.Players[0].score);
    assertEqualsInt(15, brd.Players[1].score);
    
}