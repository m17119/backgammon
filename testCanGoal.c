
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testCanGoal() {
    testStart("canGoal");
    int turn = 0;
    piece pie[30];
    
    int i = 0;
    int ans1[30] = {1,1,2,2,3,3,4,4,5,5,5,6,6,6,7,19,19,20,20,21,21,22,22,23,23,23,24,24,24,24};
    int ans2[30] = {1,1,2,2,3,3,4,4,5,5,5,6,6,6,6,18,19,20,20,21,21,22,22,23,23,23,24,24,24,24};
    int ans3[30] = {0,1,2,2,3,3,4,4,5,5,5,6,6,6,6,19,19,20,20,21,21,22,22,23,23,23,24,24,24,24};
    int ans4[30] = {1,1,2,2,3,3,4,4,5,5,5,6,6,6,7,19,19,20,20,21,21,22,22,23,23,23,24,24,24,25};
    
    for (i = 0; i < MAX_PIECES; i++) {
        pie[i].x = ans1[i];
    
    if (i<15) {
        pie[i].player = SENTE;
    } else {
        pie[i].player = GOTE;
    }
    
    }
    turn = SENTE;
    
    assertEqualsInt(0, canGoal(pie, turn));
    
    
    
    for (i = 0; i < MAX_PIECES; i++) {
        pie[i].x = ans2[i];
    
    if (i<15) {
        pie[i].player = SENTE;
    } else {
        pie[i].player = GOTE;
    }
    
    }
    
    turn = GOTE;
    
    assertEqualsInt(0, canGoal(pie, turn));
    
    for (i = 0; i < MAX_PIECES; i++) {
        pie[i].x = ans3[i];
    
    if (i<15) {
        pie[i].player = SENTE;
    } else {
        pie[i].player = GOTE;
    }
        
    }
    
    turn = SENTE;
    
    assertEqualsInt(1, canGoal(pie, turn));
    
    
    
    for (i = 0; i < MAX_PIECES; i++) {
        pie[i].x = ans4[i];
    
    if (i<15) {
        pie[i].player = SENTE;
    } else {
        pie[i].player = GOTE;
    }
        
    }
    
    turn = GOTE;
    
    assertEqualsInt(1, canGoal(pie, turn));
    
}
