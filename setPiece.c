#include <stdio.h>

#include "BackGammon.h"

int setPiece(piece *piep, int x, char num, int startNum){
    
    int i, N;
    switch (num) {
        case '.':
            N = startNum + 0;
            break;
            
        case '1':
            N = startNum + 1;
            break;
            
        case '2':
            N = startNum + 2;
            break;
        
        case '3':
            N = startNum + 3;
            break;
            
        case '4':
            N = startNum + 4;
            break;
            
        case '5':
            N = startNum + 5;
            break;
            
        case '6':
            N = startNum + 6;
            break;
            
        case '7':
            N = startNum + 7;
            break;
            
        case '8':
            N = startNum + 8;
            break;
            
        case '9':
            N = startNum + 9;
            break;
        
        case 'A':
            N = startNum + 10;
            break;
            
        case 'B':
            N = startNum + 11;
            break;
            
        case 'C':
            N = startNum + 12;
            break;
            
        case 'D':
            N = startNum + 13;
            break;
            
        case 'E':
            N = startNum + 14;
            break;
            
        case 'F':
            N = startNum + 15;
            break;
        
    }
    
    if (N > 30) {
        return -1;
    }
    
    for (i = startNum; i < N; i++) {
        (piep+i)->x = x;
    }

    return N;
}
