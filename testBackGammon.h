#ifndef __TEST_BackGammon_h
#define __TEST_BackGammon_h

#include <stdbool.h>

// **** テスト関数のプロトタイプ宣言集

void testSetTurn();
void testSetPiece();
void testCanMove();
void testResetPiece();
void testCheckScore();
void testIsEquals();
void testCanGoal();
void testSetBoardByMaster();
void testBlockPoint();
void testCheckDice();
void testCheckOwnPiece();
void testUseDice();
void testRollDice();
void testInitBoard();
void testAxisMove();
void testGoalMove();
void testDisplayBoard();

#endif
