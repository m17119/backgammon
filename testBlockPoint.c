#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testBlockPoint(){
	testStart("blockPoint");
	board brd;
	

	int i = 0;
    int ans1[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};

    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans1[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            
        } else {
            brd.pieces[i].player = GOTE;
            
        }
        
    }
    blockPoint(&brd);

    assertEqualsInt(-1, brd.blockFlag[1]);
    assertEqualsInt(0, brd.blockFlag[2]);
    assertEqualsInt(0, brd.blockFlag[3]);
    assertEqualsInt(0, brd.blockFlag[4]);
    assertEqualsInt(0, brd.blockFlag[5]);
    assertEqualsInt(1, brd.blockFlag[6]);
    assertEqualsInt(0, brd.blockFlag[7]);
    assertEqualsInt(1, brd.blockFlag[8]);
    assertEqualsInt(0, brd.blockFlag[9]);
    assertEqualsInt(0, brd.blockFlag[10]);
    assertEqualsInt(0, brd.blockFlag[11]);
    assertEqualsInt(-1, brd.blockFlag[12]);
    assertEqualsInt(1, brd.blockFlag[13]);
    assertEqualsInt(0, brd.blockFlag[14]);
    assertEqualsInt(0, brd.blockFlag[15]);
    assertEqualsInt(0, brd.blockFlag[16]);
    assertEqualsInt(-1, brd.blockFlag[17]);
    assertEqualsInt(0, brd.blockFlag[18]);
    assertEqualsInt(-1, brd.blockFlag[19]);
    assertEqualsInt(0, brd.blockFlag[20]);
    assertEqualsInt(0, brd.blockFlag[21]);
    assertEqualsInt(0, brd.blockFlag[22]);
    assertEqualsInt(0, brd.blockFlag[23]);
    assertEqualsInt(1, brd.blockFlag[24]);


    int ans2[30] = {0,0,3,6,6,6,7,8,11,11,13,15,24,25,25,0,9,9,12,12,12,17,19,19,19,19,25,25,25,25};
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans2[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            
        } else {
            brd.pieces[i].player = GOTE;
            
        }
        
    }
    blockPoint(&brd);

    assertEqualsInt(0, brd.blockFlag[1]);
    assertEqualsInt(0, brd.blockFlag[2]);
    assertEqualsInt(0, brd.blockFlag[3]);
    assertEqualsInt(0, brd.blockFlag[4]);
    assertEqualsInt(0, brd.blockFlag[5]);
    assertEqualsInt(1, brd.blockFlag[6]);
    assertEqualsInt(0, brd.blockFlag[7]);
    assertEqualsInt(0, brd.blockFlag[8]);
    assertEqualsInt(-1, brd.blockFlag[9]);
    assertEqualsInt(0, brd.blockFlag[10]);
    assertEqualsInt(1, brd.blockFlag[11]);
    assertEqualsInt(-1, brd.blockFlag[12]);
    assertEqualsInt(0, brd.blockFlag[13]);
    assertEqualsInt(0, brd.blockFlag[14]);
    assertEqualsInt(0, brd.blockFlag[15]);
    assertEqualsInt(0, brd.blockFlag[16]);
    assertEqualsInt(0, brd.blockFlag[17]);
    assertEqualsInt(0, brd.blockFlag[18]);
    assertEqualsInt(-1, brd.blockFlag[19]);
    assertEqualsInt(0, brd.blockFlag[20]);
    assertEqualsInt(0, brd.blockFlag[21]);
    assertEqualsInt(0, brd.blockFlag[22]);
    assertEqualsInt(0, brd.blockFlag[23]);
    assertEqualsInt(0, brd.blockFlag[24]);


    int ans3[30] = {2,2,6,6,7,8,8,8,9,10,13,13,18,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans3[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            
        } else {
            brd.pieces[i].player = GOTE;
            
        }
        
    }
    blockPoint(&brd);

    assertEqualsInt(-1, brd.blockFlag[1]);
    assertEqualsInt(1, brd.blockFlag[2]);
    assertEqualsInt(0, brd.blockFlag[3]);
    assertEqualsInt(0, brd.blockFlag[4]);
    assertEqualsInt(0, brd.blockFlag[5]);
    assertEqualsInt(1, brd.blockFlag[6]);
    assertEqualsInt(0, brd.blockFlag[7]);
    assertEqualsInt(1, brd.blockFlag[8]);
    assertEqualsInt(0, brd.blockFlag[9]);
    assertEqualsInt(0, brd.blockFlag[10]);
    assertEqualsInt(0, brd.blockFlag[11]);
    assertEqualsInt(-1, brd.blockFlag[12]);
    assertEqualsInt(1, brd.blockFlag[13]);
    assertEqualsInt(0, brd.blockFlag[14]);
    assertEqualsInt(0, brd.blockFlag[15]);
    assertEqualsInt(0, brd.blockFlag[16]);
    assertEqualsInt(-1, brd.blockFlag[17]);
    assertEqualsInt(0, brd.blockFlag[18]);
    assertEqualsInt(-1, brd.blockFlag[19]);
    assertEqualsInt(0, brd.blockFlag[20]);
    assertEqualsInt(0, brd.blockFlag[21]);
    assertEqualsInt(0, brd.blockFlag[22]);
    assertEqualsInt(0, brd.blockFlag[23]);
    assertEqualsInt(1, brd.blockFlag[24]);
}