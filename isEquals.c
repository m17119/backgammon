#include <stdio.h>

#include "BackGammon.h"

int isEquals(board *brdp, board *abrdp) {
    int Flag = 1;
    int i, j, moreSmall;
    piece *pp1 = brdp->pieces;
    piece *pp2 = abrdp->pieces;
    int forCompare1[MAX_PIECES];
    int forCompare2[MAX_PIECES];

    // 昇べき順に並べ替えるための配列を用意.
    for (i = 0; i < MAX_PIECES; i++) {
        forCompare1[i] = brdp->pieces[i].x;
        forCompare2[i] = abrdp->pieces[i].x;
    }

    /*for (i = 0; i < MAX_PIECES; i++) {
        printf("forCompare1[%d]:%d\n", i, forCompare1[i]);
        printf("forCompare2[%d]:%d\n", i, forCompare2[i]);
    }*/

    // ポイントが小さい順に並び替えるプログラム.
    // ほかの関数でも使える場面が出てきそう.
    // pp[j]とpp[j + 1]を比較.
    // pp[j]の方が大きい場合、二つを入れ替える.
    // jを一つ進める.
    // これを繰り返すことで、一番大きい値がMAX_PIECES-1に入る.
    // iを一つ減らして同じことを行う.
    // すると、二番目に大きい値がMAX_PIECES-2に入る.
    for (i = MAX_PIECES - 1; i > 0; i--) {
        for (j = 0; j < i; j++) {
            if (forCompare1[j] > forCompare1[j + 1]) {
                moreSmall = forCompare1[j + 1];
                forCompare1[j + 1] = forCompare1[j];
                forCompare1[j] = moreSmall;
            }
            if (forCompare2[j] > forCompare2[j + 1]) {
                moreSmall = forCompare2[j + 1];
                forCompare2[j + 1] = forCompare2[j];
                forCompare2[j] = moreSmall;
            }
        }
    }

    /*printf("昇べき順\n");
    for (i = 0; i < MAX_PIECES; i++) {
        printf("forCompare1[%d]:%d\n", i, forCompare1[i]);
        printf("forCompare2[%d]:%d\n", i, forCompare2[i]);
    }*/

    // 比較
    for (i = 0; i < MAX_PIECES; i++) {
        if (forCompare1[i] != forCompare2[i]) {
            Flag = 0;
        }
        if (pp1->player != pp2->player) {
            Flag = 0;
        }
        pp1++;
        pp2++;
    }
    if (brdp->turn != abrdp->turn) {
        Flag = 0;
    }
    return Flag;
}
