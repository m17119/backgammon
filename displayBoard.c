#include "BackGammon.h"

void displayBoard(board *brdp) {
    int display_x, display_y, display_w, display_h;
    int display_xForOwnPiece, display_yForOwnPiece;
    int i, j;
    int udFlag = 0;
    int setFlag = 0;
    int setFlagForOwnPiece = 0;
    piece moreSmall;
    piece forCompare[MAX_PIECES];

    // 昇べき順に並べ替えるための配列を用意.
    for (i = 0; i < MAX_PIECES; i++) {
        forCompare[i] = brdp->pieces[i];
    }

    // 昇べき順に並び替え.
    for (i = MAX_PIECES - 1; i > 0; i--) {
        for (j = 0; j < i; j++) {
            if (forCompare[j].x > forCompare[j + 1].x) {
                moreSmall = forCompare[j + 1];
                forCompare[j + 1] = forCompare[j];
                forCompare[j] = moreSmall;
            }
        }
    }

    /*for (i = 0; i < MAX_PIECES; i++) {
        printf("%d\t%d\n", forCompare[i].x, forCompare[i].player);
    }*/

    initscr();

    noecho();
    cbreak();
    timeout(0);

    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);  // 先手は白文字
    init_pair(2, COLOR_BLACK, COLOR_WHITE);  // 後手は黒文字
    init_pair(3, COLOR_RED, COLOR_WHITE);
    init_pair(4, COLOR_WHITE, COLOR_WHITE);
    bkgd(COLOR_PAIR(4));

    getmaxyx(stdscr, display_h, display_w);  // 画面サイズの取得

    // コマの表示
    display_y = display_h - 3;
    display_x = display_w / 2 + 36;
    display_yForOwnPiece = display_h - 3;
    display_xForOwnPiece = display_w / 2;
    i = 0;

    while (1) {
        if (forCompare[i].x != 0 && forCompare[i].x != 25) {
            if (udFlag == 0) {
                display_x = (display_w / 2 + 36) - 6 * (forCompare[i].x - 1);
                if (display_w / 2 - display_x >= 0) {
                    display_x = display_x - 6;
                }
                if (forCompare[i].x != forCompare[i - 1].x) {
                    display_y = display_h - 3;
                }
            } else {
                display_x = (display_w / 2 - 36) + 6 * (forCompare[i].x - 13);
                if (display_x - display_w / 2 >= 0) {
                    display_x = display_x + 6;
                }
                if (forCompare[i].x != forCompare[i - 1].x) {
                    display_y = 2;
                }
            }
            if (forCompare[i].x > 12 && setFlag == 0) {
                display_x = (display_w / 2 - 36) + 6 * (forCompare[i].x - 13);
                if (display_x - display_w / 2 >= 0) {
                    display_x = display_x + 6;
                }
                display_y = 2;
                udFlag = 1;
                setFlag = 1;
            }
            if (forCompare[i].player == SENTE) {
                attrset(COLOR_PAIR(1));
                mvaddch(display_y, display_x, '@');
                if (udFlag == 0) {
                    display_y--;
                } else {
                    display_y++;
                }
            } else {
                move(display_y, display_x);
                attrset(COLOR_PAIR(2));
                mvaddch(display_y, display_x, '@');
                if (udFlag == 0) {
                    display_y--;
                } else {
                    display_y++;
                }
            }
        } else if (forCompare[i].x == 0 && forCompare[i].player == GOTE) {
            attrset(COLOR_PAIR(2));
            mvaddch(display_yForOwnPiece, display_xForOwnPiece, '*');
            display_yForOwnPiece--;
        } else if (forCompare[i].x == 25 && forCompare[i].player == SENTE) {
            if (setFlagForOwnPiece == 0) {
                display_yForOwnPiece = 2;
                setFlagForOwnPiece = 1;
            }
            attrset(COLOR_PAIR(1));
            mvaddch(display_yForOwnPiece, display_xForOwnPiece, '*');
            display_yForOwnPiece++;
        }
        i++;
        if (i >= MAX_PIECES) {
            break;
        }
    }

    // ポイントの表示
    display_y = display_h - 2;
    display_x = display_w / 2 + 36;
    udFlag = 0;
    i = 1;

    while (1) {
        if (display_x != display_w / 2) {
            attrset(COLOR_PAIR(3));
            mvprintw(display_y, display_x, "%d", i);
            if (udFlag == 0) {
                display_x = display_x - 6;
            } else {
                display_x = display_x + 6;
            }
            i++;
        } else {
            if (display_y == display_h - 2) {
                attrset(COLOR_PAIR(2));
                mvaddstr(display_y, display_x, "0");
            } else {
                attrset(COLOR_PAIR(1));
                mvaddstr(display_y, display_x, "25");
            }

            if (udFlag == 0) {
                display_x = display_x - 6;
            } else {
                display_x = display_x + 6;
            }
        }
        if (i == 13) {
            display_x = display_w / 2 - 36;
            display_y = 1;
            udFlag = 1;
        }
        if (i > 24) {
            break;
        }
    }

    // ゴールの表示
    display_y = display_h - 2;
    display_x = display_w / 2 + 42;
    attrset(COLOR_PAIR(1));
    mvaddstr(display_y, display_x, "0(GOAL)");
    display_y = 1;
    attrset(COLOR_PAIR(2));
    mvaddstr(display_y, display_x, "25(GOAL)");

    // スコアの表示
    attrset(COLOR_PAIR(3));
    mvaddstr(2, 1, "SCORE");
    mvprintw(3, 1, "SENTE : GOTE = %d : %d", brdp->Players[0].score,
             brdp->Players[1].score);

    // ターンの表示
    attrset(COLOR_PAIR(3));
    mvprintw(5, 1, "TURN = %d", brdp->turn);

    // 出目の表示
    attrset(COLOR_PAIR(3));
    if(brdp->dices[0].diceflag == 0)
        mvprintw(10, 1, "diceNum[0] = %d", brdp->dices[0].diceNum);
    if(brdp->dices[1].diceflag == 0)
        mvprintw(11, 1, "diceNum[1] = %d", brdp->dices[1].diceNum);

    // 入力の表示
    attrset(COLOR_PAIR(3));
    mvprintw(15, 1, "uinPoint = %d", brdp->input.uinPoint);
    mvprintw(16, 1, "uinMoveNum = %d", brdp->input.uinMoveNum);

    // 説明の表示
    // attrset(COLOR_PAIR(3));
    // mvaddstr(40, 1, "[d]:Description");
    // mvaddstr(42, 1, "[q]:quit");

    // 再表示
    refresh();

    // endwinを実行すると継続して表示されないので、あえて書かない.
    // endwin();
}

