#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testSetBoardByMaster(){
	testStart("setBoardByMaster");
	board brd;
    int i = 0;

    setBoardByMaster(&brd, SENTE, "......5.3....5..........2.",
    	".2..........5....3.5......");


    for (i = 0; i < MAX_PIECES; i++) {
        if (i < 15) {
            assertEqualsInt(SENTE, brd.pieces[i].player);
        } else {
            assertEqualsInt(GOTE, brd.pieces[i].player);
        }
    }

    assertEqualsInt(SENTE, brd.turn);


    // 先手のコマ
    assertEqualsInt(6, brd.pieces[0].x);
    assertEqualsInt(6, brd.pieces[1].x);
    assertEqualsInt(6, brd.pieces[2].x);
    assertEqualsInt(6, brd.pieces[3].x);
    assertEqualsInt(6, brd.pieces[4].x);
    assertEqualsInt(8, brd.pieces[5].x);
    assertEqualsInt(8, brd.pieces[6].x);
    assertEqualsInt(8, brd.pieces[7].x);
    assertEqualsInt(13, brd.pieces[8].x);
    assertEqualsInt(13, brd.pieces[9].x);
    assertEqualsInt(13, brd.pieces[10].x);
    assertEqualsInt(13, brd.pieces[11].x);
    assertEqualsInt(13, brd.pieces[12].x);
    assertEqualsInt(24, brd.pieces[13].x);
    assertEqualsInt(24, brd.pieces[14].x);

    // 後手のコマ
    assertEqualsInt(1, brd.pieces[15].x);
    assertEqualsInt(1, brd.pieces[16].x);
    assertEqualsInt(12, brd.pieces[17].x);
    assertEqualsInt(12, brd.pieces[18].x);
    assertEqualsInt(12, brd.pieces[19].x);
    assertEqualsInt(12, brd.pieces[20].x);
    assertEqualsInt(12, brd.pieces[21].x);
    assertEqualsInt(17, brd.pieces[22].x);
    assertEqualsInt(17, brd.pieces[23].x);
    assertEqualsInt(17, brd.pieces[24].x);
    assertEqualsInt(19, brd.pieces[25].x);
    assertEqualsInt(19, brd.pieces[26].x);
    assertEqualsInt(19, brd.pieces[27].x);
    assertEqualsInt(19, brd.pieces[28].x);
    assertEqualsInt(19, brd.pieces[29].x);



	setBoardByMaster(&brd, GOTE, "2..1..311...21.1........12",
    	"1........2..3....1.4.....4");

	assertEqualsInt(GOTE, brd.turn);

    // 先手のコマ
    assertEqualsInt(0, brd.pieces[0].x);
    assertEqualsInt(0, brd.pieces[1].x);
    assertEqualsInt(3, brd.pieces[2].x);
    assertEqualsInt(6, brd.pieces[3].x);
    assertEqualsInt(6, brd.pieces[4].x);
    assertEqualsInt(6, brd.pieces[5].x);
    assertEqualsInt(7, brd.pieces[6].x);
    assertEqualsInt(8, brd.pieces[7].x);
    assertEqualsInt(12, brd.pieces[8].x);
    assertEqualsInt(12, brd.pieces[9].x);
    assertEqualsInt(13, brd.pieces[10].x);
    assertEqualsInt(15, brd.pieces[11].x);
    assertEqualsInt(24, brd.pieces[12].x);
    assertEqualsInt(25, brd.pieces[13].x);
    assertEqualsInt(25, brd.pieces[14].x);

    // 後手のコマ
    assertEqualsInt(0, brd.pieces[15].x);
    assertEqualsInt(9, brd.pieces[16].x);
    assertEqualsInt(9, brd.pieces[17].x);
    assertEqualsInt(12, brd.pieces[18].x);
    assertEqualsInt(12, brd.pieces[19].x);
    assertEqualsInt(12, brd.pieces[20].x);
    assertEqualsInt(17, brd.pieces[21].x);
    assertEqualsInt(19, brd.pieces[22].x);
    assertEqualsInt(19, brd.pieces[23].x);
    assertEqualsInt(19, brd.pieces[24].x);
    assertEqualsInt(19, brd.pieces[25].x);
    assertEqualsInt(25, brd.pieces[26].x);
    assertEqualsInt(25, brd.pieces[27].x);
    assertEqualsInt(25, brd.pieces[28].x);
    assertEqualsInt(25, brd.pieces[29].x);

    }
