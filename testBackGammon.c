#include "testBackGammon.h"

#include <stdio.h>

#include "BackGammon.h"
#include "testCommon.h"

int main() {
    printf("##### Test start #####\n");
    testSetTurn();
    printf("\n");
    testSetPiece();
    printf("\n");
    testCanMove();
    printf("\n");
    testResetPiece();
    printf("\n");
    testCheckScore();
    printf("\n");
    testDisplayBoard("\n");
    printf("\n");
    testIsEquals();
    printf("\n");
    testCanGoal();
    printf("\n");
    testSetBoardByMaster();
    printf("\n");
    testBlockPoint();
    printf("\n");
    testCheckDice();
    printf("\n");
    testCheckOwnPiece();
    printf("\n");
    testUseDice();
    printf("\n");
    testRollDice();
    printf("\n");
    testInitBoard();
    printf("\n");
    testAxisMove();
    printf("\n");
    testGoalMove();
    printf("\n");
    // 最終エラー統計処理
    testErrorCheck();
    printf("##### Test finish #####\n");
    return 0;
}
