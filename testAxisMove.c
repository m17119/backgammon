#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testAxisMove() {
    testStart("axisMove");
    board brd;

    //////////////////////////////////////////////////////////////////////////////////////////
    // 通常時のテスト
    //////////////////////////////////////////////////////////////////////////////////////////

    // 配置を初期配置に設定.
    // その他の値も初期値に設定できるので誤動作を防げる.
    initBoard(&brd);

    // 出目の設定
    brd.dices[0].diceNum = 5;
    brd.dices[1].diceNum = 6;

    // ターンの設定.
    // ゴールするかの判断に必要.
    // ブロックなどとの兼ね合いはblockPoint,
    // 実際に動かすのはmainMoveであるため.
    brd.turn = SENTE;

    // 動ける場合のテスト.
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 6;
    assertEqualsInt(1, axisMove(&brd));

    // 動ける場合のテスト．
    // 相手のコマはあるが、axisMoveの返り値は１になる.
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 5;
    assertEqualsInt(1, axisMove(&brd));

    // 動けない場合のテスト.
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 4;
    assertEqualsInt(0, axisMove(&brd));

    // ほかのポイントに対してもテスト１.
    // 相手のコマであっても、アクティビティ図通りの処理をするか.
    brd.input.uinPoint = 19;
    brd.input.uinMoveNum = 5;
    assertEqualsInt(1, axisMove(&brd));

    // ほかのポイントに対してもテスト２.
    // ゴールできないので0を返す.
    brd.input.uinPoint = 6;
    brd.input.uinMoveNum = 6;
    assertEqualsInt(0, axisMove(&brd));

    // 出目の設定
    brd.dices[0].diceNum = 1;

    // ターンによる違いのテスト.
    // 先手は普通に動ける.
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(1, axisMove(&brd));

    // ターンを変更
    brd.turn = GOTE;

    // ターンによる違いのテスト.
    // 自分のコマか相手のコマかとは無関係に判断.
    // 出目と方向で、動けるかどうかの判断.
    // この場合はゴールしてしまう判定で動けない.
    // ※実際にはそもそも相手のコマなので選べない.
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(0, axisMove(&brd));

    //////////////////////////////////////////////////////////////////////////////////////////
    // ゴール前のテスト
    //////////////////////////////////////////////////////////////////////////////////////////

    // boardの設定. とりあえずお互いのゴール前に固めた.
    setBoardByMaster(&brd, SENTE, ".232323...................", "...................323232.");

    // 出目の設定
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;

    // 動ける場合のテスト.
    // ピッタリゴールできる場合.
    brd.input.uinPoint = 3;
    brd.input.uinMoveNum = 3;
    assertEqualsInt(1, axisMove(&brd));

    // 動ける場合のテスト.
    // ゴールしない場合は、通常通りのルールに則る.
    brd.input.uinPoint = 6;
    brd.input.uinMoveNum = 5;
    assertEqualsInt(1, axisMove(&brd));

    // 動けない場合のテスト.
    // そもそも出目と違う.(誤り！)
    // ハマりポイントです．
    // 出目と違うから0なのではなく、下のテストと同様、ピッタリ動かせるコマが存在するので0.
    // これとこの下のテスト二つは、1ポイント目のコマを1動かしたいという意味で、まったく同じテスト．
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(0, axisMove(&brd));

    // 動けない場合のテスト.
    // ピッタリ動かせるコマが存在する.
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 3;
    assertEqualsInt(0, axisMove(&brd));

    // 後手についても同様のテスト
    brd.turn = GOTE;
    brd.input.uinPoint = 22;
    brd.input.uinMoveNum = 3;
    assertEqualsInt(1, axisMove(&brd));
    brd.input.uinPoint = 19;
    brd.input.uinMoveNum = 5;
    assertEqualsInt(1, axisMove(&brd));
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(0, axisMove(&brd));
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 3;
    assertEqualsInt(0, axisMove(&brd));

    // boardの設定. 
    setBoardByMaster(&brd, SENTE, "82..5.....................", ".....................5..28");

    // 出目の設定
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;

    // 動ける場合のテスト.
    // ピッタリじゃなくても動かせる.
    brd.input.uinPoint = 4;
    brd.input.uinMoveNum = 5; // 4でもまったく一緒.
    assertEqualsInt(1, axisMove(&brd));

    // 後手についても同様のテスト
    brd.turn = GOTE;
    brd.input.uinPoint = 21;
    brd.input.uinMoveNum = 5;
    assertEqualsInt(1, axisMove(&brd));

    // boardの設定. ブロックでゴールから遠いコマが動けない場合.
    setBoardByMaster(&brd, SENTE, "82....5...................", "....22.............5..3.21");

    // 出目の設定
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 2;

    // 動ける場合のテスト.
    // 普通に動かせる場合.
    // 出目かつゴール空の距離であることに注意.
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(1, axisMove(&brd));

    // 動ける場合のテスト.
    // 6ポイント目のコマは、出目1でも2でも動かせない.
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 2;
    assertEqualsInt(1, axisMove(&brd));

    // 後手についても同様のテスト
    setBoardByMaster(&brd, GOTE, "12.3..5.............22....", "...................5....28");
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    assertEqualsInt(1, axisMove(&brd));
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 2;
    assertEqualsInt(1, axisMove(&brd));

    // boardの設定. 指定した出目では動かせなくても、もう片方の出目で動かせる場合を想定.
    setBoardByMaster(&brd, SENTE, "82....5...................", "....21.............5...2.5");

    // 出目の設定
    brd.dices[0].diceNum = 2;
    brd.dices[1].diceNum = 4;

    // 動けない場合のテスト.
    // 6ポイント目のコマは、出目2では動けないが、出目4で動けるため、そちらが優先.
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 2; // 1でもまったく一緒.
    assertEqualsInt(0, axisMove(&brd));

    // 後手についても同様のテスト
    setBoardByMaster(&brd, GOTE, "5.2...5.............12....", "...................5....28");
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 2;
    assertEqualsInt(0, axisMove(&brd));
}
