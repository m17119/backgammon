#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testUseDice(){

	testStart("useDice");
	board brd;
	brd.dices[0].diceflag = 0;
    brd.dices[0].doubletflag = 1;
    brd.diceIndex = 0;
    useDice(brd.dices, brd.diceIndex);

	assertEqualsInt(0, brd.dices[0].doubletflag);
	assertEqualsInt(0, brd.dices[0].diceflag);

	useDice(brd.dices, brd.diceIndex);

	assertEqualsInt(0, brd.dices[0].doubletflag);
	assertEqualsInt(1, brd.dices[0].diceflag);

   
    brd.diceIndex = 1;
    brd.dices[1].diceflag = 0;
    brd.dices[1].doubletflag = 1;
    useDice(brd.dices, brd.diceIndex);

	assertEqualsInt(0, brd.dices[1].doubletflag);
	assertEqualsInt(0, brd.dices[1].diceflag);	
        
    useDice(brd.dices, brd.diceIndex);

	assertEqualsInt(0, brd.dices[1].doubletflag);
	assertEqualsInt(1, brd.dices[1].diceflag);



}