#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testCheckOwnPiece(){
	testStart("checkOwnPiece");
	board brd;
	setBoardByMaster(&brd, SENTE, "2..1..311...21.1........12", "1........2..3....1.4.....4");

	checkOwnPiece(brd.pieces, brd.Players);


	assertEqualsInt(1, brd.Players[0].ownedPieceFlag);
	assertEqualsInt(1, brd.Players[1].ownedPieceFlag);

	initBoard(&brd);

	checkOwnPiece(brd.pieces, brd.Players);

	assertEqualsInt(0, brd.Players[0].ownedPieceFlag);
	assertEqualsInt(0, brd.Players[1].ownedPieceFlag);
}