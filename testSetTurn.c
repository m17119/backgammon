#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testSetTurn() {
    testStart("setTurn");
    char nameA[255] = "Aさん";
    char nameB[255] = "Bさん";

    board a;
    a.dices[0].diceNum = 3;
    a.dices[1].diceNum = 4;
    setTurn(&a, nameA, nameB);
    printf("\n");
    assertEqualsString(nameB, a.Players[0].name);
    assertEqualsString(nameA, a.Players[1].name);

    a.dices[0].diceNum = 6;
    a.dices[1].diceNum = 5;
    setTurn(&a, nameA, nameB);
    assertEqualsString(nameB, a.Players[1].name);
    assertEqualsString(nameA, a.Players[0].name);
}