#include <stdio.h>

#include "BackGammon.h"

void setBoardByMaster(board *brdp,playerType turn,char *skp,char *gkp){
    int i, count = 0, startNum = 0;
	
    piece *pp = brdp->pieces;
    
    for (i = 0; i < MAX_PIECES; i++) {
        
        if (i<15) {
            brdp->pieces[i].player = SENTE;
        } else {
            brdp->pieces[i].player = GOTE;
        }
    }
    
    while (*skp != '\0') {
        startNum = setPiece(pp, count, *skp, startNum);
        skp ++;
        count ++;
    }
    
    count = 0;
    while (*gkp != '\0') {
        startNum = setPiece(pp, count, *gkp, startNum);
        gkp ++;
        count ++;
    }
    brdp->turn = turn;
    blockPoint(brdp);
    checkScore(brdp);
}
