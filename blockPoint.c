#include <stdio.h>

#include "BackGammon.h"

void blockPoint(board *brdp){
    int i, j, k;
    
    for (k=0; k < 26; k++) {
        brdp->blockFlag[k] = 0;
    }
    
        for (i = 0; i < 15; i++) {
            for (j = i+1; j < 15; j++) {
                if (brdp->pieces[i].x == brdp->pieces[j].x) {
                    brdp->blockFlag[brdp->pieces[i].x] = 1;
            }
        }
    }
    
        for (i = 15; i < 30; i++) {
            for (j = i+1; j < 30; j++) {
                if (brdp->pieces[i].x == brdp->pieces[j].x) {
                    brdp->blockFlag[brdp->pieces[i].x] = -1;
                }
            }
        }
    
    brdp->blockFlag[0] = 0;
    brdp->blockFlag[25] = 0;
}

