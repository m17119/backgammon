#include <stdio.h>

#include "BackGammon.h"

void checkDice(dice *dicp, userinput uin, int *index){
    *index = -1;
    
    if (dicp->diceNum == uin.uinMoveNum) {
        if (dicp->diceflag == 0) {
            *index = 0;
        }
    }
    if ((dicp+1)->diceNum == uin.uinMoveNum) {
        if ((dicp+1)->diceflag == 0) {
            *index = 1;
        }
    }
}
