
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testIsEquals() {
    testStart("isEquals");
    board brd, abrd;
    
    int i = 0;
    int ans1[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    int ans2[30] = {24,6,6,6,6,8,8,8,13,13,13,13,13,24,6,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans1[i];
        abrd.pieces[i].x = ans2[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            abrd.pieces[i].player = SENTE;
        } else {
            brd.pieces[i].player = GOTE;
            abrd.pieces[i].player = GOTE;
        }
        
    }
    
    brd.turn = SENTE;
    abrd.turn = SENTE;

    assertEqualsInt(1, isEquals(&brd, &abrd));
    
    int ans3[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    int ans4[30] = {6,6,6,6,8,6,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans3[i];
        abrd.pieces[i].x = ans4[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            abrd.pieces[i].player = SENTE;
        } else {
            brd.pieces[i].player = GOTE;
            abrd.pieces[i].player = GOTE;
        }
        
    }
    
    brd.turn = SENTE;
    abrd.turn = GOTE;

    assertEqualsInt(0, isEquals(&brd, &abrd));
    
    int ans5[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    int ans6[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,6,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans5[i];
        abrd.pieces[i].x = ans6[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            abrd.pieces[i].player = SENTE;
        } else {
            brd.pieces[i].player = GOTE;
            abrd.pieces[i].player = GOTE;
        }
        
    }
    
    brd.turn = SENTE;
    abrd.turn = SENTE;

    assertEqualsInt(0, isEquals(&brd, &abrd));
    
    int ans7[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,24,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    int ans8[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,24,6,1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};
    
    for (i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans7[i];
        abrd.pieces[i].x = ans8[i];
        
        if (i<15) {
            brd.pieces[i].player = SENTE;
            abrd.pieces[i].player = SENTE;
        } else {
            brd.pieces[i].player = GOTE;
            abrd.pieces[i].player = GOTE;
        }
        
    }
    
    brd.turn = SENTE;
    abrd.turn = GOTE;

    assertEqualsInt(0, isEquals(&brd, &abrd));
    
}
