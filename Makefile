# makefile for BackGammon

#### 実装の関数群のオブジェクトファイル名 \ を書くと次の行も継続できる
OBJECTS= setPiece.o canMove.o resetPiece.o checkScore.o\
        canGoal.o isEquals.o setTurn.o\
        setBoardByMaster.o blockPoint.o checkDice.o checkOwnPiece.o useDice.o\
        rollDice.o initBoard.o axisMove.o goalMove.o displayBoard.o
        

#### テストの関数群のオブジェクトファイル名 \ を書くと次の行も継続できる
TEST_OBJECTS= testSetTurn.o testSetPiece.o testCanMove.o testResetPiece.o testCheckScore.o\
        testCanGoal.o testIsEquals.o\
        testSetBoardByMaster.o testBlockPoint.o testCheckDice.o testCheckOwnPiece.o testUseDice.o\
		testRollDice.o testInitBoard.o testAxisMove.o testGoalMove.o testDisplayBoard.o
		
# 最終実行ファイル(名前を修正したら .gitignore も修正すること)
TARGET=BackGammon

# テスト実行ファイル(名前を修正したら .gitignore も修正すること)
TEST_TARGET=testBackGammon

# curses / ncurses を使うか
CURSES=on
#CURSES=off

# 最終実行ファイルの実名
TARGET_EXE=$(TARGET)$(EXE)
# ターゲット実行ファイルの実名
TEST_TARGET_EXE=$(TEST_TARGET)$(EXE)
# 実装のためのヘッダー(プロトタイム宣言、構造体宣言、定数定義を含む)
HEADER=$(TARGET).h
# 実装のメインファイル main 関数を含む
MAIN=$(TARGET).o
# テストのためのヘッダー(プロトタイム宣言)
TEST_HEADER=$(TEST_TARGET).h
# テストのメインファイル main 関数を含む
TEST_MAIN=$(TEST_TARGET).o
# テストに必要なファイル
TEST_COMMON=testCommon.o
# 必要な CFLAGS
CFLAGS=-Wall -g
# 必要なライブラリ
LIBS=-lm
CHCP=

ifeq ($(OS),Windows_NT)
	CC=gcc
	RM=cmd.exe /C del
	EXE=.exe
	CHCP=chcp 65001
else
	RM=rm -f
	EXE=
endif

ifeq ($(CURSES),on)
	ifeq ($(OS),Windows_NT)
		LIBS=PDCurses-3.9/wincon/pdcurses.a -lm
	else
		LIBS=-lncurses -lm
	endif
endif

exec: $(TARGET_EXE)
	$(CHCP)
	./$(TARGET_EXE)
test: $(TEST_TARGET_EXE)
	$(CHCP)
	./$?

$(TARGET_EXE): $(MAIN) $(OBJECTS) $(HEADER)
	$(CC) -o $@ $(CFLAGS) $(MAIN) $(OBJECTS) $(LIBS)

$(TEST_TARGET_EXE): $(TEST_MAIN) $(OBJECTS) $(TEST_OBJECTS) $(TEST_COMMON) $(HEADER) $(TEST_HEADER)
	$(CC) -o $@ $(CFLAGS) $(TEST_MAIN) $(OBJECTS) $(TEST_OBJECTS) $(TEST_COMMON) $(LIBS)

clean:
	$(RM) $(TARGET_EXE) $(TEST_TARGET_EXE) $(MAIN) $(TEST_MAIN) $(OBJECTS) $(TEST_OBJECTS) $(TEST_COMMON)
