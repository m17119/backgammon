#include <stdio.h>

#include "BackGammon.h"
void checkOwnPiece(piece *piep, player *plap){
	int i;
    int count = 0;

    for (i = 0; i < MAX_PIECES; i++) {
        if (count < 15) {
            if (piep->x == 25) {
                plap[0].ownedPieceFlag = 1;
            }
        } else {
            if (piep->x == 0) {
                plap[1].ownedPieceFlag = 1;
            }
        }
        piep++;
        count++;
    }
}