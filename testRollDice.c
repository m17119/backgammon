#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

#define N 100

void testRollDice() {
    testStart("rollDice");
    dice dic[2] = {};
    int i;
    int flag = 1;

    srand((unsigned int)time(NULL));
    for (i = 0; i < N; i++) {
        rollDice(dic);
        if (dic[0].diceNum > 6 || dic[1].diceNum > 6 || dic[0].diceNum < 1 ||
            dic[1].diceNum < 1) {
            // 出目が1 ~ 6の間に収まっていなければフラグを0に.
            flag = 0;
        }
        if (dic[0].diceNum == dic[1].diceNum) {
            if (dic[0].doubletflag == 0 || dic[1].doubletflag == 0) {
                // ゾロ目が出たとき、ゾロ目フラグが立っていなければフラグを0に.
                flag = 0;
            }
        } else {
            if (dic[0].doubletflag == 1 || dic[1].doubletflag == 1) {
                // ゾロ目でないとき、ゾロ目フラグが立っていればフラグを0に.
                flag = 0;
            }
        }
        if (dic[0].diceflag == 1 || dic[1].diceflag == 1) {
            // 使用フラグが立っていればフラグを0に.
            flag = 0;
        }

        assertEqualsInt(1, flag);

        // dicの中身を初期化.
        // initboard関数を作れば、それを実行するだけなので便利
        dic[0].doubletflag = 0;
        dic[1].doubletflag = 0;
    }
}