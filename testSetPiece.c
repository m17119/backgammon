#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testSetPiece(){
    testStart("setPiece");

    //通常時
    int startNum = 1;
    int x = 6;
    char num = '5';
    piece pie[30];
    assertEqualsInt(6, setPiece(pie, x, num, startNum));
    assertEqualsInt(6, pie[1].x);
    assertEqualsInt(6, pie[2].x);
    assertEqualsInt(6, pie[3].x);
    assertEqualsInt(6, pie[4].x);
    assertEqualsInt(6, pie[5].x);

    //ポイントが2桁の時
    startNum = 4;
    x = 8;
    num = 'A';
    assertEqualsInt(14, setPiece(pie, x, num, startNum));
    assertEqualsInt(8, pie[4].x);
    assertEqualsInt(8, pie[5].x);
    assertEqualsInt(8, pie[6].x);
    assertEqualsInt(8, pie[7].x);
    assertEqualsInt(8, pie[8].x);
    assertEqualsInt(8, pie[9].x);
    assertEqualsInt(8, pie[10].x);
    assertEqualsInt(8, pie[11].x);
    assertEqualsInt(8, pie[12].x);
    assertEqualsInt(8, pie[13].x);

    //構造体があふれる時
    startNum = 25;
    x = 6;
    num = '9';
    assertEqualsInt(-1, setPiece(pie, x, num, startNum));

}