#include "BackGammon.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

void description();
void startGame();
void mainMove(board *brdp);
int avoidError(board brd);

int main() {
    board brd;
    int loopFlag = 0;
    int key;
    char str1[256];
    char str2[256];
    int errorFlag = 0;
    int superFlag = 0;

    initscr();
    curs_set(0);
    echo();
    timeout(0);

    // 初期ボードの設定
    initBoard(&brd);

    // スタート画面の表示
    clear();
    startGame();

    // プレイヤー名の設定
    clear();
    mvaddstr(10, 10, "Please enter player1's name.");
    refresh();
    scanw("%s", str1);
    mvaddstr(12, 10, "Please enter player2's name.");
    refresh();
    scanw("%s", str2);
    srand((unsigned int)time(NULL));
    rollDice(brd.dices);
    setTurn(&brd, str1, str2);
    mvprintw(20, 10, "%s is \"SENTE\".", brd.Players[0].name);
    mvprintw(22, 10, "%s is \"GOTE\".", brd.Players[1].name);
    mvaddstr(30, 10, "Please push[Enter] key");
    refresh();
    while (1) {
        key = getch();
        if (key == '\n') {
            // 初期ボードの設定
            initBoard(&brd);
            break;
        } else if (key == 'g') {
            setBoardByMaster(&brd, SENTE, ".232323...................",
                             "...................323232.");
            break;
        }else if(key == 'a'){
            setBoardByMaster(&brd, SENTE, "E.......................1.",
                             "..................232323..");
            break;
        }else if(key == 's'){
            setBoardByMaster(&brd, SENTE, "9111111...................",
                             "...................1111119");
            break;
        }
    }

    rollDice(brd.dices);

    while (1) {
        // タイムテーブルの設定
        srand((unsigned int)time(NULL));

        // 文字、文字列の初期化
        key = 0;
        str1[0] = '\0';
        str2[0] = '\0';

        // 画面の初期化
        clear();

        // diceを振る
        if (errorFlag == 0 && superFlag == 1) {
            brd.turn = -1 * brd.turn;
            rollDice(brd.dices);
            superFlag = 0;
        }

        // boardの表示
        displayBoard(&brd);

        // uinPointの代入
        mvaddstr(25, 1, "Please choose any Point and [Enter].");
        refresh();
        timeout(-1);
        scanw("%d", &brd.input.uinPoint);
        timeout(0);

        // uinMoveNumの代入
        mvaddstr(25, 1, "Please choose any MoveNum and [Enter].");
        refresh();
        timeout(-1);
        scanw("%d", &brd.input.uinMoveNum);
        timeout(0);

        if (avoidError(brd) == 1) {
            if (canMove(&brd) == 0 || brd.input.uinPoint >= MAX_POINT) {
                // エラー文の表示
                mvaddstr(26, 1, "Error:Please set parameter again.");
                refresh();
                brd.input.uinPoint = 0;
                brd.input.uinMoveNum = 0;
                errorFlag = 1;
            } else {
                // コマの移動処理
                mainMove(&brd);
                errorFlag = 0;
            }
        } else {
            mvaddstr(26, 1, "Error:You can't move anywhere.");
            errorFlag = 0;
            superFlag = 1;
        }

        // 使用フラグの確認
        if (brd.dices[0].diceflag == 1 && brd.dices[1].diceflag == 1) {
            superFlag = 1;
        }

        // 終了コマンド or 説明
        mvaddstr(30, 1, "Please push any key.");
        refresh();
        mvaddstr(38, 1, "[Enter]:next");
        mvaddstr(40, 1, "[d]:Description");
        mvaddstr(42, 1, "[q]:quit");
        timeout(-1);
        key = getch();
        switch (key) {
            case 'q':
                loopFlag = 1;
                break;
            case 'd':
                description();
                timeout(0);
                break;
            default:
                timeout(0);
                break;
        }

        if (loopFlag == 1) {
            break;
        }
        if (brd.winner != 0) {
            break;
        }
    }

    // 勝敗の表示
    clear();
    while(1){
        if (brd.winner == 1) {
            mvprintw(20, 20, "winner is %s.", brd.Players[0].name);
            usleep(2000000);
        } else if (brd.winner == -1) {
            mvprintw(20, 20, "winner is %s.", brd.Players[1].name);
            usleep(2000000);
        }
        mvaddstr(25, 20, "finish->[f]key");
        timeout(-1);
        key = getch();
        if(key=='f')
            break;
    }
   

    // cursesの終了.
    endwin();

    return 0;
}

// 説明画面の表示関数
void description() {
    int Explain_x, Explain_y;
    int key;

    initscr();

    while (1) {
        clear();
        Explain_x = 10;
        Explain_y = 5;
        start_color();
        init_pair(1, COLOR_BLACK, COLOR_CYAN);
        bkgd(COLOR_PAIR(1));
        attrset(COLOR_PAIR(1));
        mvaddstr(Explain_y, Explain_x, "Description");
        Explain_y++;
        Explain_y++;
        mvaddstr(Explain_y, Explain_x, "1.[q]:You can quit this game!");
        Explain_y++;
        mvaddstr(Explain_y, Explain_x,
                 "2.[q] or [Enter]:This description will be closed.");
        Explain_y++;
        mvaddstr(Explain_y, Explain_x, "3.\"@\" is pieces on the board.");
        Explain_y++;
        mvaddstr(Explain_y, Explain_x, "4.\"*\" is own pieces.");
        Explain_y++;
        mvaddstr(Explain_y, Explain_x,
                 "5.White is \"SENTE\", Black is \"GOTE\".");
        key = getch();
        if (key == 'q' || key == '\n') {
            break;
        }

        // 再表示
        refresh();
    }

    // endwinを実行すると継続して表示されないので、あえて書かない.
    // endwin();
}

// スタート画面の表示関数
void startGame() {
    int start_x1, start_x2, start_y, start_w, start_h;
    int key;
    char *str1 = "BACKGAMMON";
    char *str2 = "Please push [Enter] key";

    initscr();

    getmaxyx(stdscr, start_h, start_w);  // 画面サイズの取得

    while (1) {
        clear();
        start_x1 = (start_w - strlen(str1)) / 2;
        start_x2 = (start_w - strlen(str2)) / 2;
        start_y = start_h / 2;
        start_color();
        init_pair(1, COLOR_BLACK, COLOR_CYAN);
        bkgd(COLOR_PAIR(1));
        attrset(COLOR_PAIR(1));
        mvaddstr(start_y, start_x1, str1);
        start_y++;
        start_y++;
        mvaddstr(start_y, start_x2, str2);
        key = getch();
        if (key == '\n') {
            break;
        }

        // 再表示
        refresh();
    }

    // endwinを実行すると継続して表示されないので、あえて書かない.
    // endwin();
}

// mainMove関数
void mainMove(board *brdp) {
    resetPiece(brdp);
    useDice(brdp->dices, brdp->diceIndex);
    checkScore(brdp);
    blockPoint(brdp);
}

int avoidError(board brd) {
    int i;
    int j;
    int flag = 0;
    board brdForCanMove;

    brdForCanMove = brd;
    for (i = 0; i < MAX_POINT; i++) {
        for (j = 0; j < MAX_PIECES; j++) {
            brdForCanMove = brd;
            brdForCanMove.input.uinPoint = i;
            brdForCanMove.input.uinMoveNum = j;
            if (canMove(&brdForCanMove) == 1) {
                flag = 1;
            }
        }
    }
    return flag;
}

