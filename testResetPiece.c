#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testResetPiece(){
	testStart("resetPiece");
    board brd,abrd;
    
    //SENTEの時の盤面が等しい
    initBoard(&brd);
    brd.turn = SENTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 3;
    brd.input.uinPoint = 24;
    resetPiece(&brd);
    setBoardByMaster(&abrd, SENTE, "......5.3....5.......1..1.",
    	".2..........5....3.5......");
    assertEqualsInt(1,isEquals(&brd,&abrd));

    //SENTEがGOTEのコマを取った時
    brd.turn = SENTE;
    setBoardByMaster(&brd, SENTE, "......5.3....5..........2.",
    	".2.........14....3.5......");
    brd.dices[0].diceNum = 2;
    brd.dices[1].diceNum = 1;
    brd.input.uinMoveNum = 2;
    brd.input.uinPoint = 13;
    resetPiece(&brd);
    setBoardByMaster(&abrd, SENTE, "......5.3..1.4..........2.",
    	"12..........4....3.5......");
    assertEqualsInt(1,isEquals(&brd,&abrd));

    //GOTEの時の盤面が等しい
    setBoardByMaster(&brd, GOTE,"......5.3....5..........2.",
    	".2..........5....3.5......");
    brd.turn = GOTE;
    brd.dices[0].diceNum = 4;
    brd.dices[1].diceNum = 6;
    brd.input.uinMoveNum = 4;
    brd.input.uinPoint = 17;
    resetPiece(&brd);
    setBoardByMaster(&abrd, GOTE, "......5.3....5..........2.",
    	".2..........5....2.5.1....");
    assertEqualsInt(1,isEquals(&brd,&abrd));

    //GOTEがSENTEのコマを取った時
    brd.turn = GOTE;
    setBoardByMaster(&brd, GOTE, "......5.3....5.1........1.",
    	".2..........5....3.5......");
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 1;
    brd.input.uinMoveNum = 3;
    brd.input.uinPoint = 12;
    resetPiece(&brd);
    setBoardByMaster(&abrd, GOTE, "......5.3....5..........11",
    	".2..........4..1.3.5......");
    assertEqualsInt(1,isEquals(&brd,&abrd));

    
}