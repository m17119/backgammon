#include <stdio.h>

#include "BackGammon.h"
int canMove(board *brdp){
    int i;
    int moveFlag = 1;
    int notPieceFlag = 0;
    
    // 選択されたポイントに自分のコマが存在するか
    for (i = 0; i < MAX_PIECES; i++) {
        if (brdp->pieces[i].x == brdp->input.uinPoint) {
            if (brdp->pieces[i].player != brdp->turn) {
                moveFlag = 0;
            }
        } else {
            notPieceFlag++;
        }
    }
    if (notPieceFlag >= MAX_PIECES) {
        moveFlag = 0;
    }
    
    // axisMove関数による判定
    if (axisMove(brdp) == 0) {
        moveFlag = 0;
    }
    
    // 持ちコマがあるか. 指定されたコマが持ちコマか.
    for (i = 0; i < MAX_PIECES; i++) {
        if (brdp->turn == SENTE) {
            if (brdp->pieces[i].x == 25) {
                if (brdp->pieces[i].x != brdp->input.uinPoint) {
                    if(brdp->pieces[i].player == brdp->turn){
                        moveFlag = 0;
                    }
                        
                }
            }
        } else {
            if (brdp->pieces[i].x == 0) {
                if (brdp->pieces[i].x != brdp->input.uinPoint) {
                    if(brdp->pieces[i].player == brdp->turn){
                        moveFlag = 0;
                    }
                }
            }
        }
    }
    
    // 移動先に相手のコマが二つ以上あるか.
    if (brdp->turn == SENTE) {
        if (brdp->blockFlag[brdp->input.uinPoint - brdp->turn * brdp->input.uinMoveNum] == -1) {
            moveFlag = 0;
        }
    } else {
        if (brdp->blockFlag[brdp->input.uinPoint - brdp->turn * brdp->input.uinMoveNum] == 1) {
            moveFlag = 0;
        }
    }
    
    return moveFlag;
}

