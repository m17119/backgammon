#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BackGammon.h"

void setTurn(board *brdp, char *nameA, char *nameB){
        
        int a = brdp->dices[0].diceNum;
        int b = brdp->dices[1].diceNum;
        
        if(a>b){
        
            strcpy(brdp->Players[0].name, nameA);
            strcpy(brdp->Players[1].name, nameB);
            printf("先手は%sです。", nameA);
            
        }else {
            
            strcpy(brdp->Players[0].name, nameB);
            strcpy(brdp->Players[1].name, nameA);
            printf("先手は%sです。", nameB);
            
        }
        
    
}
