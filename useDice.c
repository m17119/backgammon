#include <stdio.h>

#include "BackGammon.h"

void useDice(dice *dicp, int index){
    if ((dicp+index)->doubletflag == 0) {
        (dicp+index)->diceflag = 1;
    } else {
        (dicp+index)->doubletflag = 0;
    }
}
