#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testInitBoard() {
    testStart("initBoard");
    board brd;
    int i = 0;

    // 間違えて作っちゃっいました.　もったいないので残しておきます.
    // 初期piecesの代入のためのプログラムです.
    // initBoard関数の実装の際に使えると思います.
    // 最終的にはsetBoardByStrings関数で代入するようにしてもよいと思います.
    // pointはポイントの値を表します.
    // index1, index2はpiecesのインデックス("[]"内の数字)を表します.
    // index1の初期値は0, index2の初期値は15に設定します.
    /*while (1) {
        switch (point) {
            case 1:
                for (i = 0; i < 2; i++) {
                    testPieces[index2].x = point;
                    testPieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 6:
                for (i = 0; i < 5; i++) {
                    testPieces[index1].x = point;
                    testPieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 8:
                for (i = 0; i < 3; i++) {
                    testPieces[index1].x = point;
                    testPieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 12:
                for (i = 0; i < 5; i++) {
                    testPieces[index2].x = point;
                    testPieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 13:
                for (i = 0; i < 5; i++) {
                    testPieces[index1].x = point;
                    testPieces[index1].player = SENTE;
                    index1++;
                }
                break;
            case 17:
                for (i = 0; i < 3; i++) {
                    testPieces[index2].x = point;
                    testPieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 19:
                for (i = 0; i < 5; i++) {
                    testPieces[index2].x = point;
                    testPieces[index2].player = GOTE;
                    index2++;
                }
                break;
            case 24:
                for (i = 0; i < 2; i++) {
                    testPieces[index1].x = point;
                    testPieces[index1].player = SENTE;
                    index1++;
                }
                break;
        }
        if (point == 24) {
            break;
        }
        point++;
    }*/

    initBoard(&brd);

    // 最初の15コマが先手、後の15コマが後手になっているかの確認.
    for (i = 0; i < MAX_PIECES; i++) {
        if (i < 15) {
            assertEqualsInt(SENTE, brd.pieces[i].player);
        } else {
            assertEqualsInt(GOTE, brd.pieces[i].player);
        }
    }

    // コマのポイントが正しく設定されているかの確認.

    // 先手のコマ
    assertEqualsInt(6, brd.pieces[0].x);
    assertEqualsInt(6, brd.pieces[1].x);
    assertEqualsInt(6, brd.pieces[2].x);
    assertEqualsInt(6, brd.pieces[3].x);
    assertEqualsInt(6, brd.pieces[4].x);
    assertEqualsInt(8, brd.pieces[5].x);
    assertEqualsInt(8, brd.pieces[6].x);
    assertEqualsInt(8, brd.pieces[7].x);
    assertEqualsInt(13, brd.pieces[8].x);
    assertEqualsInt(13, brd.pieces[9].x);
    assertEqualsInt(13, brd.pieces[10].x);
    assertEqualsInt(13, brd.pieces[11].x);
    assertEqualsInt(13, brd.pieces[12].x);
    assertEqualsInt(24, brd.pieces[13].x);
    assertEqualsInt(24, brd.pieces[14].x);

    // 後手のコマ
    assertEqualsInt(1, brd.pieces[15].x);
    assertEqualsInt(1, brd.pieces[16].x);
    assertEqualsInt(12, brd.pieces[17].x);
    assertEqualsInt(12, brd.pieces[18].x);
    assertEqualsInt(12, brd.pieces[19].x);
    assertEqualsInt(12, brd.pieces[20].x);
    assertEqualsInt(12, brd.pieces[21].x);
    assertEqualsInt(17, brd.pieces[22].x);
    assertEqualsInt(17, brd.pieces[23].x);
    assertEqualsInt(17, brd.pieces[24].x);
    assertEqualsInt(19, brd.pieces[25].x);
    assertEqualsInt(19, brd.pieces[26].x);
    assertEqualsInt(19, brd.pieces[27].x);
    assertEqualsInt(19, brd.pieces[28].x);
    assertEqualsInt(19, brd.pieces[29].x);

    // playersが正しく設定されているかの確認.
    assertEqualsInt(SENTE, brd.Players[0].direction);
    assertEqualsInt(GOTE, brd.Players[1].direction);
    assertEqualsInt(0, brd.Players[0].score);
    assertEqualsInt(0, brd.Players[1].score);

    // blockFlagが正しく設定されているかの確認.

    // コマがあるポイント.先手は1, 後手は-1.
    assertEqualsInt(-1, brd.blockFlag[1]);
    assertEqualsInt(1, brd.blockFlag[6]);
    assertEqualsInt(1, brd.blockFlag[8]);
    assertEqualsInt(-1, brd.blockFlag[12]);
    assertEqualsInt(1, brd.blockFlag[13]);
    assertEqualsInt(-1, brd.blockFlag[17]);
    assertEqualsInt(-1, brd.blockFlag[19]);
    assertEqualsInt(1, brd.blockFlag[24]);

    // コマがないポイント.
    assertEqualsInt(0, brd.blockFlag[0]);
    assertEqualsInt(0, brd.blockFlag[2]);
    assertEqualsInt(0, brd.blockFlag[3]);
    assertEqualsInt(0, brd.blockFlag[4]);
    assertEqualsInt(0, brd.blockFlag[5]);
    assertEqualsInt(0, brd.blockFlag[7]);
    assertEqualsInt(0, brd.blockFlag[9]);
    assertEqualsInt(0, brd.blockFlag[10]);
    assertEqualsInt(0, brd.blockFlag[11]);
    assertEqualsInt(0, brd.blockFlag[14]);
    assertEqualsInt(0, brd.blockFlag[15]);
    assertEqualsInt(0, brd.blockFlag[16]);
    assertEqualsInt(0, brd.blockFlag[18]);
    assertEqualsInt(0, brd.blockFlag[20]);
    assertEqualsInt(0, brd.blockFlag[21]);
    assertEqualsInt(0, brd.blockFlag[22]);
    assertEqualsInt(0, brd.blockFlag[23]);
    assertEqualsInt(0, brd.blockFlag[25]);

    // userInputが正しく設定されているかの確認
    assertEqualsInt(0, brd.input.uinMoveNum);
    assertEqualsInt(0, brd.input.uinPoint);

    // turnが正しく設定されているかの確認
    assertEqualsInt(SENTE, brd.turn);

    // diceが正しく設定されているかの確認
    assertEqualsInt(0, brd.dices[0].diceNum);
    assertEqualsInt(0, brd.dices[0].diceflag);
    assertEqualsInt(0, brd.dices[0].doubletflag);
    assertEqualsInt(0, brd.dices[1].diceNum);
    assertEqualsInt(0, brd.dices[1].diceflag);
    assertEqualsInt(0, brd.dices[1].doubletflag);

    // ownedPieceFlagが正しく設定されているかの確認
    assertEqualsInt(0, brd.Players[0].ownedPieceFlag);
    assertEqualsInt(0, brd.Players[1].ownedPieceFlag);

    assertEqualsInt(0, brd.winner);
    assertEqualsInt(0, brd.diceIndex);
}
