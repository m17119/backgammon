#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testGoalMove() {
    testStart("goalMove");
    board brd;

    // 行うテストはaxisMoveのコピペです．
    // ただし、ゴール前で、かつゴールするときのみのテストになります．
    // 出目を書き換える場合があるので、テストごとに出目を設定し直しています.
    // テストはすべて、uinMoveNumがどちらかの出目の値であると想定して記述しています.

    // boardの設定.
    setBoardByMaster(&brd, SENTE, ".232323...................",
                     "...................323232.");
    blockPoint(&brd);

    // 動ける場合のテスト.
    // ピッタリゴールできる場合.
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 3;
    brd.input.uinMoveNum = 3;
    goalMove(&brd);
    assertEqualsInt(3, brd.input.uinMoveNum);
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);

    // 動けない場合のテスト.
    // ピッタリ動かせるコマが存在する場合１.
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 1;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);

    // 動けない場合のテスト.
    // ピッタリ動かせるコマが存在する場合２.
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 3;
    goalMove(&brd);
    assertEqualsInt(1,
                    brd.input.uinMoveNum);  // uinMoveNumが書き換わっているか.
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);

    // 後手についても同様のテスト.
    brd.turn = GOTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 22;
    brd.input.uinMoveNum = 3;
    goalMove(&brd);
    assertEqualsInt(3, brd.input.uinMoveNum);
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 3;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(5, brd.dices[1].diceNum);

    // boardの設定.
    setBoardByMaster(&brd, SENTE, "82..5.....................",
                     ".....................5..28");
    blockPoint(&brd);

    // 動ける場合のテスト.
    // ピッタリじゃなくても動かせる.
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinPoint = 4;
    brd.input.uinMoveNum = 5;
    goalMove(&brd);
    assertEqualsInt(4,
                    brd.input.uinMoveNum);  // uinMoveNumが書き換わっているか.
    assertEqualsInt(3, brd.dices[0].diceNum);
    assertEqualsInt(
        4, brd.dices[1].diceNum);  // 大きい方のdiceNumが書き換わっているか.

    // 後手についても同様のテスト.
    // 出目の順番を変えた場合のテストも兼ねる.
    brd.turn = GOTE;
    brd.dices[0].diceNum = 5;
    brd.dices[1].diceNum = 3;
    brd.input.uinPoint = 21;
    brd.input.uinMoveNum = 5;
    goalMove(&brd);
    assertEqualsInt(4, brd.input.uinMoveNum);
    assertEqualsInt(4, brd.dices[0].diceNum);
    assertEqualsInt(3, brd.dices[1].diceNum);

    // boardの設定. ブロックでゴールから遠いコマが動けない場合.
    // ゴールからの距離と等しい出目があるため、diceNumを書き換えていないように見えることに注意.
    setBoardByMaster(&brd, SENTE, "82....5...................",
                     "....22.............5..3.21");
    blockPoint(&brd);

    // 動ける場合のテスト.
    // 普通に動かせる場合.
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 2;
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 1;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(1, brd.dices[0].diceNum);
    assertEqualsInt(2, brd.dices[1].diceNum);

    // 動ける場合のテスト.
    // 6ポイント目のコマは、出目1でも2でも動かせない.
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 2;
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 2;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(1, brd.dices[0].diceNum);
    assertEqualsInt(2, brd.dices[1].diceNum);

    // 後手についても同様のテスト.
    setBoardByMaster(&brd, GOTE, "12.3..5.............22....",
                     "...................5....28");
    blockPoint(&brd);
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 2;
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 1;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(1, brd.dices[0].diceNum);
    assertEqualsInt(2, brd.dices[1].diceNum);
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 2;
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 2;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(1, brd.dices[0].diceNum);
    assertEqualsInt(2, brd.dices[1].diceNum);

    // boardの設定.
    // 指定した出目では動かせなくても、もう片方の出目で動かせる場合を想定.
    setBoardByMaster(&brd, SENTE, "82....5...................",
                     "....21.............5...2.5");
    blockPoint(&brd);

    // 動けない場合のテスト.
    // 6ポイント目のコマは、出目2では動けないが、出目4で動けるため、そちらが優先.
    brd.dices[0].diceNum = 2;
    brd.dices[1].diceNum = 4;
    brd.input.uinPoint = 1;
    brd.input.uinMoveNum = 2;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(2, brd.dices[0].diceNum);
    assertEqualsInt(4, brd.dices[1].diceNum);

    // 後手についても同様のテスト.
    // uinMoveNumを変えた場合のテストも兼ねる.
    setBoardByMaster(&brd, GOTE, "5.2...5.............12....",
                     "...................5....28");
    blockPoint(&brd);
    brd.dices[0].diceNum = 2;
    brd.dices[1].diceNum = 4;
    brd.input.uinPoint = 24;
    brd.input.uinMoveNum = 4;
    goalMove(&brd);
    assertEqualsInt(1, brd.input.uinMoveNum);
    assertEqualsInt(2, brd.dices[0].diceNum);
    assertEqualsInt(4, brd.dices[1].diceNum);
}