#include <stdio.h>
#include "BackGammon.h"

int axisMove(board *brdp) {
    if (canGoal(brdp->pieces, brdp->turn) == 1) {
        if (brdp->turn == SENTE) {
            if (brdp->input.uinPoint - brdp->input.uinMoveNum <= 0){
                goalMove(brdp);
            }
            
        } else {
            if (brdp->input.uinPoint + brdp->input.uinMoveNum >= 25){
                goalMove(brdp);
            }
        }
        
        checkDice(brdp->dices, brdp->input, &(brdp->diceIndex));
        brdp->dices[0].diceNum = brdp->bdice[0];
        brdp->dices[1].diceNum = brdp->bdice[1];
        if (brdp->diceIndex == -1) {
            return 0;
        }else{
            return 1;
        }
        
    }else{
        if (brdp->turn == SENTE) {
            if (brdp->input.uinPoint - brdp->input.uinMoveNum <= 0){
                return 0;
            }
            
        } else {
            if (brdp->input.uinPoint + brdp->input.uinMoveNum >= 25){
                return 0;
            }
        }
        checkDice(brdp->dices, brdp->input, &(brdp->diceIndex));
        if (brdp->diceIndex == -1) {
            return 0;
        }else{
            return 1;
        }
    }
        
    return 0;
}
