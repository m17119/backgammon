#include <stdio.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testCanMove(){
    testStart("canMove");
    board brd;

    //選択されたポインタに自分の駒がない
    initBoard(&brd);
    brd.turn = SENTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 3;
    brd.input.uinPoint = 5;
    assertEqualsInt(0,canMove(&brd));

    //axisMoveが通らない
    initBoard(&brd);
    brd.turn = SENTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 4;
    assertEqualsInt(0,canMove(&brd));

    //axisMoveが通る、持ち駒がない、blockPointが通る
    initBoard(&brd);
    brd.turn = SENTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 3;
    brd.input.uinPoint = 6;
    assertEqualsInt(1,canMove(&brd));

    //axisMoveが通る、持ち駒がない、blockPointが通らない
    initBoard(&brd);
    brd.turn = SENTE;
    brd.dices[0].diceNum = 2;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 5;
    brd.input.uinPoint = 6;
    assertEqualsInt(0,canMove(&brd));

    //axisMoveが通る、持ち駒がある、持ち駒が指定されない
    int ans[30] = {6,6,6,6,6,8,8,8,13,13,13,13,13,13,25,
    1,1,12,12,12,12,12,17,17,17,19,19,19,19,19};

    for (int i = 0; i < MAX_PIECES; i++) {
        brd.pieces[i].x = ans[i];
    
    if (i<15) {
        brd.pieces[i].player = SENTE;
        } else {
        brd.pieces[i].player = GOTE;
        }   
    }

    brd.turn = SENTE;
    brd.dices[0].diceNum = 3;
    brd.dices[1].diceNum = 5;
    brd.input.uinMoveNum = 3;
    brd.input.uinPoint = 6;
    assertEqualsInt(0,canMove(&brd));

    //axisMoveが通る、持ち駒がある、持ち駒が指定される、blockPointが通る
    brd.input.uinPoint = 25;
    assertEqualsInt(1,canMove(&brd));

    //axisMoveが通る、持ち駒がある、持ち駒が指定される、blockPointが通らない
    brd.dices[0].diceNum = 1;
    brd.dices[1].diceNum = 6;
    brd.input.uinMoveNum = 6;
    brd.input.uinPoint = 25;
    assertEqualsInt(0,canMove(&brd));

}