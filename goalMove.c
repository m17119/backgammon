#include <stdio.h>

#include "BackGammon.h"

int nearPointer(piece *pp, int refPoint, int turn) {
    int nearPoint;
    int i;
    if (turn == SENTE) {
        nearPoint = MAX_POINT;
        for (i = 0; i < 15; i++) {
            if (pp->x > refPoint) {
                if (nearPoint > pp->x) {
                    nearPoint = pp->x;
                }
            }
            pp++;
        }

    } else {
        nearPoint = 0;
        pp += 15;
        for (i = 0; i < 15; i++) {
            if (pp->x < refPoint) {
                if (nearPoint < pp->x) {
                    nearPoint = pp->x;
                }
            }
            pp++;
        }
    }
    return nearPoint;
}

void goalMove(board *brdp) {
    brdp->bdice[0] = brdp->dices[0].diceNum;
    brdp->bdice[1] = brdp->dices[1].diceNum;
    // goalPointを定義
    int goalPoint;
    if (brdp->turn == SENTE) {
        goalPoint = 0;
    } else {
        goalPoint = 25;
    }

    //ゴールからの距離として再定義
    brdp->input.uinMoveNum = goalPoint + brdp->turn * brdp->input.uinPoint;
    // printf("%d\n", brdp->input.uinMoveNum);

    // nearPointを定義
    int nearPoint = nearPointer(brdp->pieces, brdp->input.uinPoint, brdp->turn);

    // もし指定されたコマが最も遠いポイントにあるならnearPointはそのコマのポイントとする
    // また、その情報をfarthestに記憶しておく
    int farthest = 0;
    if (nearPoint == MAX_POINT || nearPoint == 0) {
        nearPoint = brdp->input.uinPoint;
        farthest = 1;
    }

    // parameterを定義
    int parameter = goalPoint + brdp->turn * nearPoint;

    // A,Bを定義
    int A = brdp->dices[0].diceNum - parameter;
    int B = brdp->dices[1].diceNum - parameter;
    int BP1, BP2;

    int Flag = 1;

    while (1) {
        if (A >= 0 || B >= 0) {
            Flag = 0;
            break;
        } else {
            BP1 = nearPoint - brdp->turn * brdp->dices[0].diceNum;
            BP2 = nearPoint - brdp->turn * brdp->dices[1].diceNum;
            if (brdp->blockFlag[BP1] == -brdp->turn &&
                brdp->blockFlag[BP2] == -brdp->turn) {
                Flag = 1;
                nearPoint = nearPointer(brdp->pieces, nearPoint, brdp->turn);
                if (nearPoint == MAX_POINT || nearPoint == 0) {
                    break;
                }
                parameter = goalPoint + brdp->turn * nearPoint;
                A = brdp->dices[0].diceNum - parameter;
                B = brdp->dices[1].diceNum - parameter;
            } else {
                Flag = 0;
                break;
            }
        }
    }

    // 指定されたコマが最も遠いポイントにあった場合に動作
    // 移動先がゴールで、かつ指定されたコマが最も遠い場合は必ず動ける
    // よって、Flagを1に書き換える
    if (farthest == 1) {
        Flag = 1;
    }

    // Flagが1なら小さい方の出目を書き換える
    // 小さい方の出目で足りない場合は大きい方
    if (Flag == 1) {
        if (brdp->dices[0].diceNum <= brdp->dices[1].diceNum) {
            if (brdp->input.uinMoveNum <= brdp->dices[0].diceNum) {
                brdp->dices[0].diceNum = brdp->input.uinMoveNum;
            } else if(brdp->input.uinMoveNum <= brdp->dices[1].diceNum){
                brdp->dices[1].diceNum = brdp->input.uinMoveNum;
            }        
        } else {
            if (brdp->input.uinMoveNum <= brdp->dices[1].diceNum) {
                brdp->dices[1].diceNum = brdp->input.uinMoveNum;
            } else if(brdp->input.uinMoveNum <= brdp->dices[0].diceNum){
                brdp->dices[0].diceNum = brdp->input.uinMoveNum;
            }
        }
    }
}

