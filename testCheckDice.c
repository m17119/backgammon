#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "BackGammon.h"
#include "testBackGammon.h"
#include "testCommon.h"

void testCheckDice(){
	 testStart("checkDice");
	
	 board brd;
     brd.dices[0].diceflag = 0;
     brd.dices[1].diceflag = 0;
	 brd.dices[0].diceNum=1;
	 brd.dices[1].diceNum=3;

	
	 brd.input.uinMoveNum=1;

	 checkDice(brd.dices, brd.input, &(brd.diceIndex));
	 assertEqualsInt(0, brd.diceIndex);

	 brd.input.uinMoveNum=3;
	 checkDice(brd.dices, brd.input, &(brd.diceIndex));
	 assertEqualsInt(1, brd.diceIndex); 

	 brd.input.uinMoveNum=5;
	 checkDice(brd.dices, brd.input, &(brd.diceIndex));
	 assertEqualsInt(-1, brd.diceIndex); 

	 brd.dices[0].diceflag=1;

	 brd.input.uinMoveNum=1;
	 checkDice(brd.dices, brd.input, &(brd.diceIndex));
	 assertEqualsInt(-1, brd.diceIndex);
	
}
